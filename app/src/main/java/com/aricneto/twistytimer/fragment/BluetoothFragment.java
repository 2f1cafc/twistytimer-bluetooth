package com.aricneto.twistytimer.fragment;

import com.aricneto.twistytimer.activity.MainActivity;

import java.util.List;
import java.util.ArrayList;

import com.aricneto.twistytimer.smart.SmartCubeHandler;
import com.aricneto.twistytimer.smart.SmartCubeListener;

import com.aricneto.twistify.R;

import android.content.Context;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import android.annotation.SuppressLint;

import android.location.LocationManager;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanResult;
import android.bluetooth.BluetoothDevice;

import android.util.Log;

import android.os.Bundle;
import android.os.Handler;

import android.view.View;
import android.widget.TextView;
import android.widget.ListView;
import android.widget.AdapterView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import android.content.pm.PackageManager;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class BluetoothFragment extends BaseFragment implements SmartCubeListener {

    private BluetoothAdapter mBluetoothAdapter;
    private BluetoothLeScanner mBluetoothLeScanner;
    private LocationManager mLocationManager;

    private boolean mScanning = false;
    private Handler mHandler;

    private final String TAG = "bluetooth";

    private static final long SCAN_PERIOD = 30000;

    private Unbinder mUnbinder;

    @BindView(R.id.bluetoothStatus) TextView btStatusTxt;
    @BindView(R.id.cubeState) TextView btCubeStateTxt;
    @BindView(R.id.btConnect) TextView btConnectBtn;
    @BindView(R.id.btScan) TextView btScanBtn;
    @BindView(R.id.btDeviceList) ListView btDeviceList;

    List<BluetoothDevice> devices;
    DeviceListAdapter deviceListAdapter;

    private SmartCubeHandler getSmartCubeHandler() {
        return ((MainActivity) getActivity()).smartCubeHandler;
    }

    private void setTextView(TextView tv, String text) {
        // needs to be run in the main ui thread
        getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    tv.setText(text);
                }
            });
    }

    private void setCubeStateText(String text) { setTextView(btCubeStateTxt, text); }
    private void setStatusText(String text) { setTextView(btStatusTxt, text); }

    private void setupScan(long timeout) {
        if(timeout != 0) {
            mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Log.d(TAG, "Scan timeout");
                        teardownScan();
                    }
                }, timeout);
        }
        mScanning = true;
        btScanBtn.setEnabled(false);
        devices.clear();
        btDeviceList.invalidateViews();
        btDeviceList.setVisibility(View.VISIBLE);
        mBluetoothLeScanner.startScan(scanCallback);
    }

    private void setupScan() { setupScan(SCAN_PERIOD); }

    private void teardownScan() {
        mBluetoothLeScanner.stopScan(scanCallback);

        mScanning = false;
        btScanBtn.setEnabled(true);
    }

    private void connectToCube(BluetoothDevice device) {
        SmartCubeHandler handler = getSmartCubeHandler();
        Log.d(TAG, "connecting to cube: " + device.getName() + "; " + device.getAddress());
        handler.connectToCube(device);
        handler.addListener(this);
    }

    private final View.OnClickListener connectBtnClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!getContext().getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
                    Log.e(TAG, "Bluetooth le is not supported");
                    return;
                }

                Log.d(TAG, "connect button clicked");
            }
        };

    private ScanCallback scanCallback = new ScanCallback() {
            @Override
            public void onScanResult(int callbackType, ScanResult result) {
                Log.d(TAG, "onScanResult: " + result.getDevice().getName());
                addBluetoothDevice(result.getDevice());
            }

            @Override
            public void onBatchScanResults(List<ScanResult> results) {
                for(ScanResult result : results){
                    Log.d(TAG, "onScanBatchResult: " + result.getDevice().getName());
                    addBluetoothDevice(result.getDevice());
                }
            }

            @Override
            public void onScanFailed(int errorCode) {
                Log.d(TAG, "onScanFailed: " + errorCode);
                // Toast.makeText(Main.this, "onScanFailed: " + String.valueOf(errorCode),
                //         Toast.LENGTH_LONG).show();
            }

            private void addBluetoothDevice(BluetoothDevice device) {
                if(getSmartCubeHandler().isSupportedDevice(device) && !devices.contains(device)) {
                    devices.add(device);
                    Log.d(TAG, "Adding device: " + device.getName() + "; " + devices.size());
                    btDeviceList.invalidateViews();
                }
            }

        };

    private final View.OnClickListener scanBtnClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "scan button clicked");
                setupScan();
            }
        };

    private final AdapterView.OnItemClickListener deviceListItemClickListener =
        new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long idl) {
                final BluetoothDevice device = (BluetoothDevice) parent.getItemAtPosition(position);
                Log.d(TAG, "clicked on: " + device.getName() + "; " + device.getAddress());

                connectToCube(device);

                teardownScan();
                btDeviceList.setVisibility(View.INVISIBLE);

                // final Intent intent = new Intent(Main.this, DetailsActivity.class);
                // intent.putExtra(DetailsActivity.EXTRAS_DEVICE_NAME,  device.getName());
                // intent.putExtra(DetailsActivity.EXTRAS_DEVICE_ADDRESS, device.getAddress());

                // if (mScanning) {
                //     mBluetoothLeScanner.stopScan(scanCallback);
                //     mScanning = false;
                //     scanButton.setEnabled(true);
                // }
                // startActivity(intent);
            }
        };

    public static BluetoothFragment newInstance() {
        return new BluetoothFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mHandler = new Handler();
        getBluetoothAdapterAndLeScanner();

        devices = new ArrayList<>();
        deviceListAdapter = new DeviceListAdapter(getContext(), android.R.layout.simple_list_item_1, devices);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        btConnectBtn.setOnClickListener(connectBtnClickListener);
        btScanBtn.setOnClickListener(scanBtnClickListener);
        btDeviceList.setAdapter(deviceListAdapter);
        btDeviceList.setOnItemClickListener(deviceListItemClickListener);
    }

    private void getBluetoothAdapterAndLeScanner(){
        final BluetoothManager bluetoothManager = (BluetoothManager) getContext().getSystemService(Context.BLUETOOTH_SERVICE);
        assert bluetoothManager != null;
        mBluetoothAdapter = bluetoothManager.getAdapter();
        mBluetoothLeScanner = mBluetoothAdapter.getBluetoothLeScanner();
        mLocationManager = (LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE);

        mScanning = false;
    }

    @SuppressLint({"ClickableViewAccessibility", "RestrictedApi"})
    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_bluetooth, container, false);

        mUnbinder = ButterKnife.bind(this, root);

        return root;
    }

    public void onStateChanged(SmartCubeHandler h) {
        // String scramble = h.captureScramble();
        Log.d(TAG, "smart cube state changed; last move: " + h.getLastMove());
        setCubeStateText(h.captureScramble());
    }
}

class DeviceListAdapter extends ArrayAdapter<BluetoothDevice> {
    DeviceListAdapter(Context context, int resource, List<BluetoothDevice> items) {
        super(context, resource, items);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;

        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(android.R.layout.simple_list_item_1, null);
        }
        final BluetoothDevice device = getItem(position);
        TextView tt1 = v.findViewById(android.R.id.text1);
        tt1.setText(device.getName());

        return v;
    }

}
