package com.aricneto.twistytimer.smart;
import java.util.Vector;
import puzzle.ThreeByThreeCubePuzzle;
import android.bluetooth.BluetoothDevice;

public abstract class SmartCubeHandler {
    protected final String TAG = "bluetooth";

    abstract public ThreeByThreeCubePuzzle.CubeState getCubeState();
    abstract public String getLastMove();

    private boolean connected = false;

    private Vector<SmartCubeListener> listeners;

    public boolean isConnected() { return connected; }
    protected void setConnected(boolean connected) { this.connected = connected; }

    protected SmartCubeHandler() {
        listeners = new Vector<SmartCubeListener>();
    }

    // after the main app has scanned for bluetooth devices, if it
    // finds one it will pass it to us to handle the actual connection
    abstract public void connectToCube(BluetoothDevice device);
    // main app will use this to filter the list of devices it shows
    // when scanning to only those that can actually be connected to.
    abstract public boolean isSupportedDevice(BluetoothDevice device);

    public void addListener(SmartCubeListener l) {
        listeners.add(l);
    }

    public void removeListener(SmartCubeListener l) {
        listeners.remove(l);
    }

    // called by a concrete sub-class when it knows the cube state has
    // changed.
    protected void updateState() {
        for(SmartCubeListener l : listeners) {
            l.onStateChanged(this);
        }
    }

    static String invertAlgorithm(String alg) {
        String[] parts = alg.split(" ");
        String result = "";
        int j = 0;
        for(int i = parts.length - 1; i >= 0; i--) {
            String part = parts[i];
            result += part.charAt(0);
            if(part.length() == 1) result += "'";
            else if(part.charAt(1) == '2') result += "2";
            if(i != 0) result += " ";
        }
        return result;
    }

    public String captureScramble() {
        // returns the scramble that will result in the current state of the cube.
        ThreeByThreeCubePuzzle.CubeState state = getCubeState();
        return invertAlgorithm(state.solveIn(20));
    }
}
