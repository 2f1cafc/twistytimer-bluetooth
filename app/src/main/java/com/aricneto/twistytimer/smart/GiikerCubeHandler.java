package com.aricneto.twistytimer.smart;
import puzzle.ThreeByThreeCubePuzzle;
import puzzle.CubePuzzle;
import puzzle.CubePuzzle.Face;
import java.security.SecureRandom;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Logger;
import java.util.logging.Level;
import fi.vexu.supercubeapi.SuperCube;
import fi.vexu.supercubeapi.SuperCubeListener;
import android.util.Log;
import android.bluetooth.BluetoothDevice;
import android.content.Context;

import android.util.Log;

public class GiikerCubeHandler extends SmartCubeHandler {
    private Context context;

    byte[] state;

    public GiikerCubeHandler(Context ctx) {
        super();
        this.context = ctx;
    }

    private final int CUBE_SIZE = 3;

    private SuperCube cubeConnection;

    private final int R = Face.R.ordinal();
    private final int U = Face.U.ordinal();
    private final int F = Face.F.ordinal();
    private final int L = Face.L.ordinal();
    private final int D = Face.D.ordinal();
    private final int B = Face.B.ordinal();

    private final int[][] cornerOrientations = {
        { 1, 2, 0 },
        { 2, 0, 1 },
        { 0, 1, 2 }
    };

    private final int[][][] corners = {
        { { D, 0, 2 }, { R, 2, 0 }, { F, 2, 2 } },
        { { R, 0, 0 }, { U, 2, 2 }, { F, 0, 2 } },
        { { U, 2, 0 }, { L, 0, 2 }, { F, 0, 0 } },
        { { L, 2, 2 }, { D, 0, 0 }, { F, 2, 0 } },
        { { R, 2, 2 }, { D, 2, 2 }, { B, 2, 0 } },
        { { U, 0, 2 }, { R, 0, 2 }, { B, 0, 0 } },
        { { L, 0, 0 }, { U, 0, 0 }, { B, 0, 2 } },
        { { D, 2, 0 }, { L, 2, 0 }, { B, 2, 2 } }
    };

    private final int[][][] edges = {
        { { F, 2, 1 }, { D, 0, 1 } },
        { { F, 1, 2 }, { R, 1, 0 } },
        { { F, 0, 1 }, { U, 2, 1 } },
        { { F, 1, 0 }, { L, 1, 2 } },
        { { D, 1, 2 }, { R, 2, 1 } },
        { { U, 1, 2 }, { R, 0, 1 } },
        { { U, 1, 0 }, { L, 0, 1 } },
        { { D, 1, 0 }, { L, 2, 1 } },
        { { B, 2, 1 }, { D, 2, 1 } },
        { { B, 1, 0 }, { R, 1, 2 } },
        { { B, 0, 1 }, { U, 0, 1 } },
        { { B, 1, 2 }, { L, 1, 0 } }
    };

    private final int[][] edgeOrientations = { { 0, 1 }, { 1, 0 } };

    private void insertCorner(int[][][] image, int cornerIdx, int posIdx, int orientIdx) {
        int[][] corner = corners[cornerIdx-1];
        int[][] pos = corners[posIdx-1];

        if (orientIdx != 3) {
            if (posIdx == 1 || posIdx == 3 || posIdx == 6 || posIdx == 8) {
                orientIdx = 3 - orientIdx;
            }
        }

        int[] orientation = cornerOrientations[orientIdx-1];

        // set each of the three faces:
        for(int faceIdx = 0; faceIdx < corner.length; faceIdx++) {
            int[] posFace = pos[faceIdx];
            image[posFace[0]][posFace[1]][posFace[2]] = corner[orientation[faceIdx]][0];
        }
    }

    private void insertEdge(int[][][] image, int edgeIdx, int posIdx, boolean orientIdx) {
        int[][] edge = edges[edgeIdx-1];
        int[][] pos = edges[posIdx-1];

        int[] orientation = edgeOrientations[orientIdx ? 0 : 1];

        for(int faceIdx = 0; faceIdx < edge.length; faceIdx++) {
            int[] posFace = pos[faceIdx];
            image[posFace[0]][posFace[1]][posFace[2]] = edge[orientation[faceIdx]][0];
        }
    }

    private int hi(Byte b) { return (b >>> 4) & 0xF; }
    private int lo(Byte b) { return b & 0xF; }

    private void insertCorners(int[][][] image, byte[] state) {
        for(int c = 0; c < 4; c++) {
            insertCorner(image, hi(state[c]), c * 2 + 1, hi(state[c + 4]));
            insertCorner(image, lo(state[c]), c * 2 + 2, lo(state[c + 4]));
        }
    }

    private void insertEdges(int[][][] image, byte[] state) {
        int orientations = (state[14] << 4) | hi(state[15]); // 12 bits defining orientation 0 or 1
        for(int e = 8; e < 14; e++) {
            int edgeNum = (e-8) * 2;
            insertEdge(image, hi(state[e]), edgeNum + 1, (orientations & (1 << (11 - edgeNum))) == 0);
            insertEdge(image, lo(state[e]), edgeNum + 2, (orientations & (1 << (11 - (edgeNum + 1)))) == 0);
        }
    }

    public ThreeByThreeCubePuzzle.CubeState getCubeState() {
        int[][][] image = new int[6][CUBE_SIZE][CUBE_SIZE];

        // initialise the center pieces as they aren't part of the state, naturally
        for(int face = 0; face < 6; face++) image[face][1][1] = face;

        insertCorners(image, state);
        insertEdges(image, state);

        return new ThreeByThreeCubePuzzle().new CubeState(image);
    }

    private char[] faceNames = { 'B', 'D', 'L', 'U', 'R', 'F' };

    public String getLastMove() {
        char face = faceNames[hi(state[16])-1];
        int turnIdx = lo(state[16])-1;

        switch(turnIdx) {
        case 1:
            return face + "2";
        case 2:
            return face + "'";
        case 8:
            return face + "2'";
        default:
            return face + "";
        }
    }

    private final SuperCubeListener cubeListener = new SuperCubeListener() {
            public void onStatusReceived() {
                state = getCubeConnection().getCubeState();
                String stateStr = SuperCube.bytesToString(state);
                Log.d(TAG, stateStr);
                updateState();
            };
            public void onBatteryReceived(int battery) {
                Log.d(TAG, "cube battery: " + battery);
            }
            public void onMovesReceived(int moves) {
                Log.d(TAG, "cube total moves: " + moves);
            }
            public void onResetReceived(byte[] response) {}
            public void onOtherReceived(byte[] response) {}
            public void onCubeReady() {
                Log.d(TAG, "Cube READY");
            }
            public void onConnectionStateUpdated() {
                Log.d(TAG, "Cube connection state: " + getCubeConnection().getConnectionState());
                // setStatusText(cubeConnection.getConnectionState());
            }
        };

    public void setCubeConnection(SuperCube conn) {
        cubeConnection = conn;
        conn.setSuperCubeListener(cubeListener);
    }

    public SuperCube getCubeConnection() {
        return cubeConnection;
    }

    public void connectToCube(BluetoothDevice device) {
        Log.d(TAG, "Connecting to GIIKER cube: " + device.getName());
        SuperCube cubeConnection =
            new SuperCube(device.getName(), device.getAddress());
        setCubeConnection(cubeConnection);
        cubeConnection.connect(context);
        Log.d(TAG, "GIIKER cube connection state: " + cubeConnection.getConnectionState());
    }

    public boolean isSupportedDevice(BluetoothDevice device) {
        return device.getName() != null && device.getName().indexOf("Gi") == 0;
    }

}
