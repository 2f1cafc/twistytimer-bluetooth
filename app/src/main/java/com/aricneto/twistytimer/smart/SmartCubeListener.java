package com.aricneto.twistytimer.smart;

public interface SmartCubeListener {
    public void onStateChanged(SmartCubeHandler h);
}
